package org.apache.celestial.repo;

import org.apache.celestial.entity.IotLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface IotLogRepository extends JpaRepository<IotLog, Long>, JpaSpecificationExecutor<IotLog> {



}
