package org.apache.celestial;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class CelestialApplication {

    public static void main(String[] args) {
        SpringApplication.run(CelestialApplication.class, args);
        try {
            while (Thread.activeCount() > 1) {
                Thread.sleep(Integer.MAX_VALUE);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
