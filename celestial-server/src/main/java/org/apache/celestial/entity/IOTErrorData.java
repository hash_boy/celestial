package org.apache.celestial.entity;


import java.time.LocalDateTime;


public class IOTErrorData {

	private Long messageId;

	private String content;

	private LocalDateTime operationTime;

	public Long getMessageId() {
		return messageId;
	}

	public void setMessageId(Long messageId) {
		this.messageId = messageId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public LocalDateTime getOperationTime() {
		return operationTime;
	}

	public void setOperationTime(LocalDateTime operationTime) {
		this.operationTime = operationTime;
	}

	@Override
	public String toString() {
		return "IOTErrorData [messageId=" + messageId + ", content=" + content + ", operationTime=" + operationTime
				+ "]";
	}

    
}
