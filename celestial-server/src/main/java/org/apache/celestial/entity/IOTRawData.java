package org.apache.celestial.entity;

import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;


public class IOTRawData {
    /*
    {"messageId":"1323829716452702208","content":"{\"deviceType\":\"CustomCategory\",\"iotId\":\"HsExzjnAgXXdBEi4t6A7000100\",\"requestId\":\"4373\",\"checkFailedData\":{},\"productKey\":\"a1IC7U0dxRo\",\"gmtCreate\":1604460574622,\"deviceName\":\"AN1001240000\",\"items\":{\"ANT_Value\":{\"value\":6.69,\"time\":1604460574625}}}","topic":"/a1IC7U0dxRo/AN1001240000/thing/event/property/post"}
     */
	private final static Logger log = LoggerFactory.getLogger(IOTRawData.class);

    private Long messageId;

    private String topic;

    private LocalDateTime operationTime;

    private IOTContent content;

    private Integer partition;

    private String comment;

    public static  IOTRawData fromString(String message) throws IOTException {
        IOTRawData iotRawData = new IOTRawData();
        if (message == null || message.length() == 0) return iotRawData ;

        JSONObject json = JSONObject.parseObject(message);
        if (json == null) {
            log.error("json format issue:{}", message);
            throw new IOTException();
        }


        iotRawData.setMessageId(json.getLong("messageId"));
        iotRawData.setTopic(json.getString("topic"));

        JSONObject contentObj = json.getJSONObject("content");
        if (contentObj == null) {
            log.error("json content format issue:{}", message);
            throw new IOTException();
        }

        IOTContent iotContent = new IOTContent();
        iotContent.setCheckFailedData(contentObj.getString("checkFailedData"));
        iotContent.setDeviceName(contentObj.getString("deviceName"));
        iotContent.setDeviceType(contentObj.getString("deviceType"));
        iotContent.setGmtCreate(contentObj.getLong("gmtCreate"));
        iotContent.setIotId(contentObj.getString("iotId"));
        iotContent.setProductKey(contentObj.getString("productKey"));
        iotContent.setRequestId(contentObj.getString("requestId"));

        JSONObject itemsObj = contentObj.getJSONObject("items");
        if (itemsObj == null) {
            log.error("json items format issue:{}", message);
            throw new IOTException();
        }

        List<IOTItem> listIOTItem = new ArrayList<IOTItem>();
		  /*
        "items\":{\"ANT_Value\":{\"value\":6.69,\"time\":1604460574625}}}
         */
        for (String item : itemsObj.keySet()) {
            IOTItem iotItem = new IOTItem();
            iotItem.setName(item);
            JSONObject itemObj = itemsObj.getJSONObject(item);
            iotItem.setValue(itemObj.getString("value"));
            iotItem.setTime(itemObj.getLong("time"));
            iotItem.setIotTime(LocalDateTime.ofInstant(Instant.ofEpochMilli(itemObj.getLong("time")), ZoneId.systemDefault()));
            listIOTItem.add(iotItem);
        }
        if (listIOTItem == null || listIOTItem.size() == 0) {
            log.error("items list is empty:{}", message);
            throw new IOTException();
        }

        iotContent.setItems(listIOTItem);
        iotRawData.setOperationTime(LocalDateTime.now());
        iotRawData.setContent(iotContent);
        return iotRawData;


    }

	public Long getMessageId() {
		return messageId;
	}

	public void setMessageId(Long messageId) {
		this.messageId = messageId;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public LocalDateTime getOperationTime() {
		return operationTime;
	}

	public void setOperationTime(LocalDateTime operationTime) {
		this.operationTime = operationTime;
	}

	public IOTContent getContent() {
		return content;
	}

	public void setContent(IOTContent content) {
		this.content = content;
	}

    public Integer getPartition() {
        return partition;
    }

    public void setPartition(Integer partition) {
        this.partition = partition;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }


    @Override
    public String toString() {
        return "IOTRawData{" +
                "messageId=" + messageId +
                ", topic='" + topic + '\'' +
                ", operationTime=" + operationTime +
                ", content=" + content +
                ", partition=" + partition +
                ", comment='" + comment + '\'' +
                '}';
    }
}
