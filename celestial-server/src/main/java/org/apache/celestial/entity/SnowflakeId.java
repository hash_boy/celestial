package org.apache.celestial.entity;

import cn.hutool.core.util.IdUtil;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.IdentifierGenerator;

import java.io.Serializable;

public class SnowflakeId implements IdentifierGenerator {

		public SnowflakeId() {
			super();
		}

		@Override
		public Serializable generate(SharedSessionContractImplementor session, Object object) {
			Serializable id = session.getEntityPersister(null, object).getClassMetadata().getIdentifier(object, session);
			if (id != null && Long.valueOf(id.toString()) > 0) {
				return id;
			} else {
				return IdUtil.getSnowflake(0, 0).nextId();
			}
		}

}