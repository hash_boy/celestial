package org.apache.celestial.entity;




import java.util.List;


public class IOTContent {
    /*
    {"messageId":"1323829716452702208","content":"{\"deviceType\":\"CustomCategory\",\"iotId\":\"HsExzjnAgXXdBEi4t6A7000100\",\"requestId\":\"4373\",\"checkFailedData\":{},\"productKey\":\"a1IC7U0dxRo\",\"gmtCreate\":1604460574622,\"deviceName\":\"AN1001240000\",\"items\":{\"ANT_Value\":{\"value\":6.69,\"time\":1604460574625}}}","topic":"/a1IC7U0dxRo/AN1001240000/thing/event/property/post"}
     */

	private String deviceType;

	private String iotId;

	private String requestId;

    private String checkFailedData;

    private String productKey;

    private Long gmtCreate;

    private String deviceName;

    private List<IOTItem> items;

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public String getIotId() {
		return iotId;
	}

	public void setIotId(String iotId) {
		this.iotId = iotId;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public String getCheckFailedData() {
		return checkFailedData;
	}

	public void setCheckFailedData(String checkFailedData) {
		this.checkFailedData = checkFailedData;
	}

	public String getProductKey() {
		return productKey;
	}

	public void setProductKey(String productKey) {
		this.productKey = productKey;
	}

	public Long getGmtCreate() {
		return gmtCreate;
	}

	public void setGmtCreate(Long gmtCreate) {
		this.gmtCreate = gmtCreate;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public List<IOTItem> getItems() {
		return items;
	}

	public void setItems(List<IOTItem> items) {
		this.items = items;
	}

	@Override
	public String toString() {
		return "IOTContent [deviceType=" + deviceType + ", iotId=" + iotId + ", requestId=" + requestId
				+ ", checkFailedData=" + checkFailedData + ", productKey=" + productKey + ", gmtCreate=" + gmtCreate
				+ ", deviceName=" + deviceName + ", items=" + items + "]";
	}





	
}
