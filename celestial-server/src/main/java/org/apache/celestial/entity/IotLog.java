package org.apache.celestial.entity;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "iot_log")
@Setter
@Getter
@ToString
public class IotLog implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(generator = "snowFlakeId")
    @GenericGenerator(name = "snowFlakeId", strategy = "org.apache.celestial.entity.SnowflakeId")
    @Column(name = "id")
    private Long id;

    @Column(name = "message_id")
    private Long messageId;

    @Column(name = "operation_time")
    private LocalDateTime operationTime;

    @Column(name = "product_key")
    private String productKey;

    @Column(name = "device_name")
    private String deviceName;

}
