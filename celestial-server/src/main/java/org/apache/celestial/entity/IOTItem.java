package org.apache.celestial.entity;

import java.time.LocalDateTime;

public class IOTItem {
    /*
    {"messageId":"1323829716452702208","content":"{\"deviceType\":\"CustomCategory\",\"iotId\":\"HsExzjnAgXXdBEi4t6A7000100\",\"requestId\":\"4373\",\"checkFailedData\":{},\"productKey\":\"a1IC7U0dxRo\",\"gmtCreate\":1604460574622,\"deviceName\":\"AN1001240000\",\"items\":{\"ANT_Value\":{\"value\":6.69,\"time\":1604460574625}}}","topic":"/a1IC7U0dxRo/AN1001240000/thing/event/property/post"}
     */

	private String name;

	private String value;

	private Long time;

	private LocalDateTime iotTime;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Long getTime() {
		return time;
	}

	public void setTime(Long time) {
		this.time = time;
	}

	public LocalDateTime getIotTime() {
		return iotTime;
	}

	public void setIotTime(LocalDateTime iotTime) {
		this.iotTime = iotTime;
	}

	@Override
	public String toString() {
		return "IOTItem [name=" + name + ", value=" + value + ", time=" + time + ", iotTime=" + iotTime + "]";
	}

	
}
