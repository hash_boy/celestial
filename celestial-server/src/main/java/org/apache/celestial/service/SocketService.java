package org.apache.celestial.service;

import org.apache.celestial.core.stream.EnvironmentContext;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.stereotype.Component;

import java.util.stream.Stream;

@Component
public class SocketService implements InitializingBean {

    @Autowired
    EnvironmentContext<String> environmentContext;

    @Override
    public void afterPropertiesSet() {
        new SimpleAsyncTaskExecutor( "socket-connector-task").submit(() ->{
//            EnvironmentContext<String> environmentContext = new EnvironmentContext<>();
            Stream<String> socketConnector = environmentContext.createSocketConnector(9911);
            socketConnector.forEach(System.out::println);
            return null;
        });
    }
}
