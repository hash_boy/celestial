package org.apache.celestial.connector;

import java.io.IOException;
import java.util.Properties;
import java.util.function.Function;
import java.util.stream.Stream;

public interface IConnector<T> {

    void init(Properties props) throws IOException, InterruptedException;

    T getData();
}
