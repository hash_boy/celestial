package com.apache.celestial.operator;

import java.util.Optional;
import java.util.stream.Stream;

public class StreamUtil {

    public static <IN, OUT> Stream<OUT> join(Stream<IN> origin, Stream<OUT> addtion) {

        return origin.map(ina -> {
            Optional<OUT> any = addtion.filter(inb -> ina.equals(inb)).findAny();
            if (any.isPresent()) return any.get();
            else return null;
        }).filter(in -> in != null);

    }


    public static void main(String[] args) {

        Stream<Integer> streamA = Stream.of(1, 2, 3, 4, 5, 6);
        Stream<Integer> streamB = Stream.of(4, 6);
        System.out.println(streamA.getClass());
        Stream<Integer> join = join(streamA, streamB);
        join.forEach(System.out::println);

    }

}
