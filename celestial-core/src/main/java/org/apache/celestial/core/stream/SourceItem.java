package org.apache.celestial.core.stream;

public interface SourceItem {

    void init();

    void create();

}
