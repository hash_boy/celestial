package org.apache.celestial.core.stream;

import java.util.function.Function;

public abstract class StreamItem<T> {

    protected StreamItem<T> nextItem;

    protected Function<T,T> func;

    public StreamItem(Function<T,T> func) {
        this.func = func;
    }

    public StreamItem(StreamItem nextItem, Function<T,T> func) {
        this.nextItem = nextItem;
        this.func = func;
    }

    protected void process(T t){
        T apply = func.apply(init(t));
        if (nextItem != null)  nextItem.process(apply);
    }

    abstract T init(T t);

}
