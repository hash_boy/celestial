package org.apache.celestial.core.stream;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Configuration implements Serializable,Cloneable {

    private Map<String,Object> config = new HashMap<>();

    public  Object getProperty(String key) {
        return config.get(key);
    }

    public void setProperty(String key,Object value) {
        config.put(key,value);
    }

    public Map<String, Object> getConfig() {
        return config;
    }
}
